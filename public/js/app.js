var gaApp = function() {
	"use strict";
	var favorites = [];
	loadUI();
	loadFavorites();
	
	function loadUI() {
		getElement("search-text").onkeyup = searchByTitle;
		setOnclick("search-result", showDetail);
	}
	
	function showDetail() {
		show("detail");
		hide("main");
	}
	
	function showMain() {
		hide("detail");
		show("main");
		// refresh the search data - in case we're returning from a favorite
		searchByTitle();
	}

	function showFavorite(imdbID) {
		searchByImdbID(imdbID);
		showDetail();
	}

	function isInFavorites(imdbID) {
		return favorites.find(function(obj) {return obj.imdbID == imdbID;});
	}

	function populateFavorites(favArray) {
		/*
		 * Build DOM for '<p><a href="#" onclick="gaApp.showFavorite(favorite.imdbID)">' + favorite.Title + '</a></p>';
		 */
		var favoritesDiv = getElement("favorites");
		
		// remove any existing children (favorites from div)
		while (favoritesDiv.firstChild) {
			favoritesDiv.removeChild(favoritesDiv.firstChild);
		}

		// visit each fav. ignore paras returned; use map() to create closure for each loop
		var paras = favArray.map(function(f) {
			var favPara = document.createElement("p");
			var a = document.createElement('a');
			var linkText = document.createTextNode(f.Title);
			a.appendChild(linkText);
			a.title = f.Title;
			a.href = "#";
			a.onclick = function() {
				showFavorite(f.imdbID);
				return false;
			};
			favPara.appendChild(a);
			favoritesDiv.appendChild(favPara);
		});
	}

	function acceptFavorites(responseText) {
		console.log('responseText:' + responseText);
		var favArray = JSON.parse(responseText);
		populateFavorites(favArray);
		favorites = favArray;
	}

	function loadFavorites() {
		var xhttp = new XMLHttpRequest();
		var url = "/favorites";

		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4) {
				if (xhttp.status == 200) {
					acceptFavorites(xhttp.responseText);
				}
			}
		};
		xhttp.open("GET", url, true);
		xhttp.send();
	}

	function addToFavorites(title, imdbID) {
		var favoriteData = {Title: title, imdbID: imdbID};

		var xhttp = new XMLHttpRequest();
		xhttp.open("POST", "/favorites", true);
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4) {
				if (xhttp.status == 200) {
					acceptFavorites(xhttp.responseText);
				}
			}
		};
		xhttp.setRequestHeader("Content-type", "application/json");
		xhttp.send(JSON.stringify(favoriteData));
	}

	function searchByTitle() {
		var target = getElement("search-text").value;
		var url = "https://www.omdbapi.com/?t={}&y=&r=json".replace('{}', target);
		doSearch(url, function(data) {
			updateSearchResult(data);
			populateDetail(data);
		});
	}
	
	function searchByImdbID(imdbID) {
		console.log('search imdbID:' + imdbID);
		var url = "https://www.omdbapi.com/?i={}&plot=full&r=json".replace('{}', imdbID);
		doSearch(url, function(data) {
			populateDetail(data);
		});
	}
	
	function doSearch(url, populate) {
		console.log('search url:' + url);
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4) {
				console.log("status: " + xhttp.status);
				if (xhttp.status == 200) {
					console.log('response:' + xhttp.responseText);
					var data = JSON.parse(xhttp.responseText);
					populate(data);
				}
			}
		};
		xhttp.open("GET", url, true);
		xhttp.send();
	}

	function updateSearchResult(obj) {
		if (obj.Response == "False") {
			console.log("Search error: " + obj.Error);
			hide("search-result");

		}
		else {
			var title = obj.Title;
			console.log('title:' + title);
			setText("search-result", title);
			show("search-result");
		}
	}

	function populateDetail(obj) {	
		setLabeledText("title", obj.Title);
		setLabeledText("actors", obj.Actors);
		setLabeledText("writer", obj.Writer);
		setLabeledText("awards", obj.Awards);
		setLabeledText("country", obj.Country);
		setLabeledText("director", obj.Director);
		setLabeledText("genre", obj.Genre);
		setLabeledText("language", obj.Language);
		setLabeledText("metascore", obj.Metascore);
		setLabeledText("plot", obj.Plot);
		setImageSource("poster", obj.Poster);
		setLabeledText("rated", obj.Rated);
		setLabeledText("released", obj.Released);
		setLabeledText("response", obj.Response);
		setLabeledText("runtime", obj.Runtime);
		setLabeledText("type", obj.Type);
		setLabeledText("year", obj.Year);
		setLabeledText("imdbID", obj.imdbID);
		setLabeledText("imdbRating", obj.imdbRating);
		setLabeledText("imdbVotes", obj.imdbVotes);
		
		setOnclick("back-to-search", showMain);
		if (isInFavorites(obj.imdbID)) {
			getElement("add-to-favorites").disabled = true;
		}
		else {
			getElement("add-to-favorites").disabled = false;
			setOnclick("add-to-favorites", function() {
				addToFavorites(obj.Title, obj.imdbID);
				showMain();
			});
		}
	}

	/*
	 * Without jQuery, add simple utilities to set element attributes
	 */
	function setText(id, text) {
		getElement(id).innerHTML = text;
	}

	function setImageSource(id, src) {
		getElement(id).src = src;
	}

	function setOnclick(id, fn) {
		getElement(id).onclick = fn;
	}

	function show(id) {
		getElement(id).style.display = 'block';
	}

	function hide(id) {
		getElement(id).style.display = 'none';
	}

	/*
	 * Specific to this app, set element text along with its name (id capitalized)
	 */
	function setLabeledText(id, text) {
		var e = getElement(id);
		if (e) {
			e.innerHTML = capitalize(id) + ":&nbsp;" + text;
		}
	}

	function getElement(id) {
		return document.getElementById(id);
	}

	function capitalize(s) {
		return s && s[0].toUpperCase() + s.slice(1);
	}

};