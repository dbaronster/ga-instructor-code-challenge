![GA Logo](https://raw.github.com/generalassembly/ga-ruby-on-rails-for-devs/master/images/ga.png)

## Doug Baron's Instructor Code Challenge

### Goal 

> Create a single page application that will utilize an external API to request movie data. The client side application will be served by a back-end which will have the ability to persist data.

#### Front-end Complete

- Uses Vanilla Javascript.

- The page should has a form that uses the [OMDBapi](http://www.omdbapi.com/) to search for matching movies and then display the results.
 - *Example*: If a user searches for `Star Wars`, the single Star Wars movie that OMDB now returns is displayed.

- When the user clicks on a search result, detailed information about that movie is displayed.

- Users can "favorite" a movie and have it persisted via the provided back-end.

- Link to favorited movies are displayed.

#### Deliverables

- Link to a [git repo with the completed code challenge](https://bitbucket.org/dbaronster/ga-instructor-code-challenge)

- Link to the application [deployed on Heroku](https://mighty-beach-54860.herokuapp.com/)

#### Bonus

- Added logic to prevent a movie from being Favoited more than once.