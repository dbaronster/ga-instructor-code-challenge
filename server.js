var bodyParser = require('body-parser');
var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var FAVORITES_FILE = './data.json';

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.set('port', process.env.PORT || 3030);

app.get('/', function(req, res){
  res.send("got hello");
});

app.get('/favorites', function(req, res){
  var data = fs.readFileSync(FAVORITES_FILE);
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

// curl -X PUT -H "Content-Type: application/json" -d '{"Title":"Sample Title","imdbID":"tt2170593"}' http://localhost:3030/favorites
app.post('/favorites', function(req, res){
  if(!req.body.Title || !req.body.imdbID){
    res.send("Error");
    return;
  }
  
  var data = JSON.parse(fs.readFileSync(FAVORITES_FILE));
  data.push(req.body);
  fs.writeFile(FAVORITES_FILE, JSON.stringify(data));
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

app.listen(app.get('port'), function(){
  console.log("Listening on port " + app.get('port'));
});